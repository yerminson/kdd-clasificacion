=== Run information ===

Scheme:weka.classifiers.bayes.NaiveBayes 
Relation:     data_study-weka.filters.unsupervised.attribute.Remove-R13-weka.filters.unsupervised.attribute.Discretize-F-B5-M0.0-R1-weka.filters.unsupervised.attribute.ReplaceMissingValues-weka.filters.unsupervised.attribute.Remove-R3-weka.filters.unsupervised.attribute.Discretize-F-B5-M0.0-R11
Instances:    12426
Attributes:   12
              age
              workclass
              education_num
              marital-status
              occupation
              relationship
              race
              sex
              capital-gain
              capital-loss
              hours-per-week
              class-attribute
Test mode:split 66.0% train, remainder test

=== Classifier model (full training set) ===

Naive Bayes Classifier

                              Class
Attribute                     false      true
                             (0.76)    (0.24)
==============================================
age
  '(-inf-25.5]'               2425.0      48.0
  '(25.5-33.5]'               2171.0     434.0
  '(33.5-41.5]'               1765.0     803.0
  '(41.5-50.5]'               1458.0     928.0
  '(50.5-inf)'                1645.0     759.0
  [total]                     9464.0    2972.0

workclass
  Private                     7411.0    1944.0
  Self-emp-not-inc             695.0     277.0
  Self-emp-inc                 193.0     237.0
  Federal-gov                  221.0     136.0
  Local-gov                    561.0     236.0
  State-gov                    380.0     143.0
  Without-pay                    3.0       1.0
  Never-worked                   3.0       1.0
  [total]                     9467.0    2975.0

education_num
  mean                        9.6088    11.547
  std. dev.                   2.4131    2.4166
  weight sum                    9459      2967
  precision                        1         1

marital-status
  Married-civ-spouse          3159.0    2534.0
  Divorced                    1543.0     180.0
  Never-married               3892.0     184.0
  Separated                    378.0      28.0
  Widowed                      334.0      29.0
  Married-spouse-absent        152.0      16.0
  Married-AF-spouse              8.0       3.0
  [total]                     9466.0    2974.0

occupation
  Tech-support                 236.0     113.0
  Craft-repair                1175.0     359.0
  Other-service               1224.0      56.0
  Sales                       1037.0     375.0
  Exec-managerial              778.0     728.0
  Prof-specialty              1528.0     771.0
  Handlers-cleaners            468.0      35.0
  Machine-op-inspct            667.0     102.0
  Adm-clerical                1301.0     189.0
  Farming-fishing              331.0      39.0
  Transport-moving             489.0     129.0
  Priv-house-serv               59.0       1.0
  Protective-serv              177.0      83.0
  Armed-Forces                   3.0       1.0
  [total]                     9473.0    2981.0

relationship
  Wife                         326.0     291.0
  Own-child                   1915.0      21.0
  Husband                     2771.0    2229.0
  Not-in-family               2872.0     332.0
  Other-relative               347.0      14.0
  Unmarried                   1234.0      86.0
  [total]                     9465.0    2973.0

race
  White                       7942.0    2710.0
  Asian-Pac-Islander           290.0      91.0
  Amer-Indian-Eskimo           101.0      15.0
  Other                         90.0       8.0
  Black                       1041.0     148.0
  [total]                     9464.0    2972.0

sex
  Male                        5823.0    2512.0
  Female                      3638.0     457.0
  [total]                     9461.0    2969.0

capital-gain
  mean                      155.9845 3875.6123
  std. dev.                1070.8354  14315.43
  weight sum                    9459      2967
  precision                 943.3868  943.3868

capital-loss
  mean                       54.2928  201.5178
  std. dev.                 311.4151  600.9743
  weight sum                    9459      2967
  precision                  56.5714   56.5714

hours-per-week
  '(-inf-35.5]'               2378.0     218.0
  '(35.5-39.5]'                288.0      63.0
  '(39.5-40.5]'               4578.0    1239.0
  '(40.5-50.5]'               1357.0     897.0
  '(50.5-inf)'                 863.0     555.0
  [total]                     9464.0    2972.0



Time taken to build model: 0.13 seconds

=== Evaluation on test split ===
=== Summary ===

Correctly Classified Instances        3497               82.7692 %
Incorrectly Classified Instances       728               17.2308 %
Kappa statistic                          0.4542
Mean absolute error                      0.1819
Root mean squared error                  0.3721
Relative absolute error                 50.1545 %
Root relative squared error             87.7472 %
Total Number of Instances             4225     

=== Detailed Accuracy By Class ===

               TP Rate   FP Rate   Precision   Recall  F-Measure   ROC Area  Class
                 0.942     0.543      0.85      0.942     0.893      0.888    false
                 0.457     0.058      0.706     0.457     0.555      0.888    true
Weighted Avg.    0.828     0.429      0.816     0.828     0.814      0.888

=== Confusion Matrix ===

    a    b   <-- classified as
 3043  189 |    a = false
  539  454 |    b = true